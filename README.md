## CKEditor 5 colgroup

This project provides a CKEditor5 plugin that defines a schema for `colgroup`
and `col` elements in tables.

This is only needed if you are not using the `table.TableColumnResize`.

### Plugin development

The plugin was created from [ckeditor5_plugin_starter_template](https://www.drupal.org/project/ckeditor5_dev)

In the module directory, run `yarn install` to set up the necessary assets.

After installing dependencies, plugin can be built with `yarn build` or `yarn
watch`. It will be built to `js/build/colgroupPlugin.js`.
