// eslint-disable-next-line import/no-extraneous-dependencies
import { Plugin } from 'ckeditor5/src/core';

class Colgroup extends Plugin {
  init() {
    this._defineSchema();
    this._defineConverters();
  }

  _defineSchema() {
    // Schemas are registered via the central `editor` object.
    const { schema } = this.editor.model;

    schema.register('tableColumnGroup', {
      allowIn: 'table',
      allowAttributes: ['colSpan'],
      isLimit: true,
    });

    schema.register('tableColumn', {
      allowIn: 'tableColumnGroup',
      allowAttributes: ['colSpan'],
      isLimit: true,
    });
  }

  _defineConverters() {
    // Converters are registered via the central editor object.
    const { conversion } = this.editor;

    conversion.elementToElement({
      model: 'tableColumnGroup',
      view: 'colgroup',
    });
    conversion.elementToElement({ model: 'tableColumn', view: 'col' });

    conversion.attributeToAttribute({
      model: {
        name: 'tableColumn',
        key: 'colSpan',
      },
      view: {
        name: 'col',
        key: 'span',
      },
    });

    conversion.attributeToAttribute({
      model: {
        name: 'tableColumnGroup',
        key: 'colSpan',
      },
      view: {
        name: 'colgroup',
        key: 'span',
      },
    });
  }
}

export default {
  Colgroup,
};
