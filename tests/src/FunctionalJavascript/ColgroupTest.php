<?php

namespace Drupal\Tests\ckeditor5_colgroup\FunctionalJavascript;

/**
 * Test that the colgroup plugin retains the colgroup and col elements.
 *
 * @group ckeditor5_colgroup
 */
class ColgroupTest extends UseCaseTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'ckeditor5',
    'ckeditor5_colgroup',
    'node',
    'text',
  ];

  /**
   * Confirms colgroup and col elements are retained when saved.
   */
  public function testTableConversion() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Assert the colgroup and col are present before editing the node.
    $this->drupalGet($this->host->toUrl());
    $this->assertElementsPresentOnPage();

    // Open the edit form.
    $this->drupalGet($this->host->toUrl('edit-form'));
    $this->assertElementsPresentInEditor();

    // Save the node.
    $page->pressButton('Save');
    $assert_session->waitForText('has been updated');

    // Assert the colgroup and col are still present after saving the node.
    $this->assertElementsPresentOnPage();
  }

  /**
   * Assert that the colgroup and col elements are present in the editor.
   */
  protected function assertElementsPresentInEditor(): void {
    $xpath = new \DOMXPath($this->getEditorDataAsDom());
    $this->assertNotEmpty($xpath->query('//table'), 'A <table> should be present');
    $this->assertNotEmpty($xpath->query('//table/colgroup'), 'A <colgroup> should be present');
    $this->assertNotEmpty($xpath->query('//table/colgroup/col'), 'A <col> should be present');
  }

}
