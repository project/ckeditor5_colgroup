<?php

namespace Drupal\Tests\ckeditor5_colgroup\FunctionalJavascript;

use Drupal\editor\Entity\Editor;
use Drupal\filter\Entity\FilterFormat;
use Drupal\node\NodeInterface;
use Drupal\Tests\ckeditor5\FunctionalJavascript\CKEditor5TestBase;
use Drupal\Tests\ckeditor5\Traits\CKEditor5TestTrait;

/**
 * Prove there is a use case for this module.
 *
 * This test covers when the module is not enabled, e.g. this exists to show
 * the use case for this module. If this test fails this module can be
 * considered obsolete.
 *
 * @group ckeditor5_colgroup
 */
class UseCaseTest extends CKEditor5TestBase {

  use CKEditor5TestTrait;

  /**
   * A host entity with a body field to contain tables.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected NodeInterface $host;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'ckeditor5',
    'node',
    'text',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    FilterFormat::create([
      'format' => 'test_format',
      'name' => 'Test format',
      'filters' => [
        'filter_html' => [
          'status' => TRUE,
          'settings' => [
            'allowed_html' => '<br> <p> <table> <tr> <td rowspan colspan> <th rowspan colspan scope> <thead> <tbody> <tfoot> <caption> <colgroup span class> <col span class>',
          ],
        ],
      ],
    ])->save();
    Editor::create([
      'editor' => 'ckeditor5',
      'format' => 'test_format',
      'settings' => [
        'toolbar' => [
          'items' => [
            'insertTable',
            'sourceEditing',
          ],
        ],
        'plugins' => [
          'ckeditor5_sourceEditing' => [
            'allowed_tags' => ['<colgroup class>', '<col class>', '<th scope>'],
          ],
        ],
      ],
    ])->save();

    $table = <<<EOL
<table>
  <caption>
    Superheros and sidekicks
  </caption>
  <colgroup>
    <col>
    <col span="2" class="batman" />
    <col span="2" class="flash" />
  </colgroup>
  <thead>
    <tr>
    <th> </th>
    <th scope="col">Batman</th>
    <th scope="col">Robin</th>
    <th scope="col">The Flash</th>
    <th scope="col">Kid Flash</th>
  </tr>
  </thead>
  <tbody>
      <tr>
    <th scope="row">Skill</th>
    <td>Smarts</td>
    <td>Dex, acrobat</td>
    <td>Super speed</td>
    <td>Super speed</td>
    </tr>
  </tbody>
</table>
EOL;

    // Create a sample host entity.
    $this->host = $this->createNode([
      'type' => 'page',
      'title' => 'Super heros',
      'body' => [
        'value' => $table,
        'format' => 'test_format',
      ],
    ]);
    $this->host->save();

    $this->drupalLogin($this->drupalCreateUser([
      'use text format test_format',
      'bypass node access',
    ]));
  }

  /**
   * Confirms colgroup and col elements are removed when saved.
   */
  public function testTableConversion() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Assert the colgroup and col are present before editing the node.
    $this->drupalGet($this->host->toUrl());
    $this->assertElementsPresentOnPage();

    // Open the edit form.
    $this->drupalGet($this->host->toUrl('edit-form'));
    $this->assertElementsNotPresentInEditor();

    // Save the node.
    $page->pressButton('Save');
    $assert_session->waitForText('has been updated');

    // Assert the colgroup and col is no longer present after saving the node.
    $this->assertElementsNotPresentOnPage();
  }

  /**
   * Assert that the colgroup and col elements are present on the page.
   */
  protected function assertElementsPresentOnPage(): void {
    $page = $this->getSession()->getPage();
    $this->assertNotEmpty($page->find('css', 'table > tbody > tr > td'));
    $this->assertNotEmpty($page->find('css', 'table colgroup'));
    $this->assertNotEmpty($page->find('css', 'table col.batman'));
    $this->assertNotEmpty($page->find('css', 'table col.flash'));
  }

  /**
   * Assert that the colgroup and col elements are not present on the page.
   */
  protected function assertElementsNotPresentOnPage(): void {
    $page = $this->getSession()->getPage();
    $this->assertEmpty($page->find('css', 'table colgroup'));
    $this->assertEmpty($page->find('css', 'table col'));
  }

  /**
   * Assert that the colgroup and col elements are not present in the editor.
   */
  protected function assertElementsNotPresentInEditor(): void {
    $xpath = new \DOMXPath($this->getEditorDataAsDom());
    $this->assertNotEmpty($xpath->query('//table'));
    $this->assertEmpty($xpath->query('//table/colgroup'));
  }

}
